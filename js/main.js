/*
 * 
 * Author: Alla Salah
 * Date: Nov 29th, 2017
 */

//---------------------------------------------
//Methods Used During the Signup 
//---------------------------------------------
var outputHtml="";


function redirectProductSearch() {
    window.location="productSearch.php";
}

function redirectToHome(){
     window.location="index.php?is_existing_user=true";
}


function createCustomer(){

     //console.log('jsonData: '+jsonData);
     var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {


        if (this.readyState == 4 && this.status == 200) {
            //getProductInfo(xhttp.responseText);
            redirectProductSearch();
        }
     };
        var url="http://localhost:8081/customerservice/customers";
        console.log("Create url= "+ url);
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader( "Content-Type", "application/json");
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.send(jsonData)

}
//---------------------------------------------
//          End Signup code
//---------------------------------------------
 
function searchProducts( search){
        console.log('Start of searchProduct()');
        var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            
        
            if (this.readyState == 4 && this.status == 200) {
                var jsonResponse = JSON.parse(xhttp.responseText);
                //var replaced= xhttp.responseText.replace("[","").replace(":[",":").replace(":[",":").replace("}]}","}}").slice(0,-1);                
                getProductList(jsonResponse);
            }
         };
            var url="http://localhost:8081/productservice/products/search/"+search;
            console.log("url= "+ url);
            xhttp.open("GET", url, true);
            xhttp.setRequestHeader( "Accept", "application/json");
            xhttp.send();
    }
//---------------------------------------------
//          End Signup code
//--------------------------------------------- 
  function redirectToHome(){
         window.location="index.php?is_existing_user=true";
  }
 
//---------------------------------------------
// Name: getProductList()
// Input: XMLHttpRequest.resonseText from search
// Output: A striped-table of matching products 
//---------------------------------------------
function getProductList (JSONproductsList) {
        console.log("JSON Product List: "+JSONproductsList);
        //var parsedJSON= JSON.parse(JSONproductsList);
        var productResultTable_sub1=' <table class="table table-striped"> '
            +'<thead class="blue-grey lighten-4">'
                +'<tr>'
                    +'<th>Product ID #</th>'
                    +'<th>Name</th>'
                    +'<th>Description</th>'
                   +' <th>Price</th>'
                   +' <th>Details</th>'
                +'</tr>'
            +'</thead>'
            +'<tbody>';
        
        var tableRow = '<tr>';
        console.log("JSONproductsList.length;= "+JSONproductsList.length);
        for (var i = 0; i < JSONproductsList.length; i++) {
              tableRow = tableRow +"<td>"+ JSONproductsList[i].product_id + "</td> ";
              tableRow = tableRow +"<td>"+ JSONproductsList[i].productName+"</td> "; 
              tableRow = tableRow +"<td>"+ JSONproductsList[i].productDescription+"</td> ";  
              tableRow = tableRow +"<td>"+ JSONproductsList[i].productPrice.toLocaleString('en-IN',{ style: 'currency', currency: 'USD' })+"</td> ";  
              tableRow = tableRow +"<td><form method='post' action=''><input type='hidden' name='link' value='"+JSONproductsList[i].product_id+"'/> <button type='submit'>"+JSONproductsList[i].link[0].action+"</button></form></td></tr> ";  

              //tableRow = tableRow +"<td><a href='"+JSONproductsList[i].link[0].url+"'  >"+JSONproductsList[i].link[0].action+"</a></td> ";  
              //tableRow = tableRow +"<td>"+ JSONproductsList[i].link[0].action+"- "+JSONproductsList[i].link[0].url;
            
            console.log("Product_ID= "+ JSONproductsList[i].product_id);
            console.log("link url: "+ JSONproductsList[i].link[0].url);    
        }
        tableRow += ' </tbody> </table> ';
        var completeTable=productResultTable_sub1+ tableRow;
        console.log("New completeTable: "+ completeTable);
        $('#content').html(completeTable);
    }
//------------------------------------------------------------------------------------
// Used: On the productSearch page
// Name: getProductDetail()
// Input: Accepts product ID # 
// Output:Makes an API Call to get details for product and produces a 
//        table with the product information displayed
// URL: http://localhost:8081/productservice/products/<<product ID # >>
//------------------------------------------------------------------------------------

function getProductDetail(productId){
        console.log('Start of getProductDetail()');
        var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            
        
            if (this.readyState == 4 && this.status == 200 ) {
                 console.log('getProductDetail()-- ready state');
                 var jsonResponse = JSON.parse(xhttp.responseText); 
                 var productResultTable_sub1=' <table class="table table-striped"> '
                    +'<thead class="blue-grey lighten-4">'
                        +'<tr>'
                            +'<th>Details Category</th>'
                            +'<th>Product Category Details</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody>';

                var tableRow;
                var linkAction= "" + jsonResponse.link[0].action;
                var purchaseQnty="";
                
                for(var index=0; index < jsonResponse.availableQuantity; index++){
                        purchaseQnty +="<option value='"+index+"'>"+index+"</option>";
                }
                tableRow = tableRow +"<tr><td> ID #</td><td>"+ jsonResponse.product_id+"</td></tr> "; 
                tableRow = tableRow +"<tr><td> Name</td><td> "+jsonResponse.productName+"</li></td></tr>";
                tableRow = tableRow +"<tr><td> Description</td><td>"+ jsonResponse.productDescription+"</td></tr> "; 
                tableRow = tableRow +"<tr><td> Price</td><td>"+ jsonResponse.productPrice.toLocaleString('en-IN',{ style: 'currency', currency: 'USD' })+"</td></tr> ";  
                tableRow = tableRow +"<tr><td> Quantity Available </td><td>"+ jsonResponse.availableQuantity+"</td></tr> ";  
                tableRow = tableRow +"<tr><td> Manufacturer </td><td>"+ jsonResponse.manufacturer+"</td></tr> ";  
                tableRow = tableRow +"<tr><td> Buy </td><td> <form class='form-inline  method='post'>"
                                    +"<input type='hidden' name='productId' value='"+ jsonResponse.product_id+"'/>"
                                    +"<input type='hidden' name='productPrice' value='"+jsonResponse.productPrice+"'/>"
                                    +"<input type='hidden' name='customerId' value='"+customerId+"'/>"
                                    +"<input type='hidden' name='customerAddrId' value='80'/>"
                                    +"<input type='hidden' name='isCanceled' value='false'/>"
                                    +"<input type='hidden' name='orderTimestamp' value=''/>"
                                    +"<input type='hidden' name='orderStatus' value='Processing'/>"
                                    +"<div class='form-check mb-2 mr-sm-2 mb-sm-0'>"
                                    +"<select name='qntyOrdered' class='custom-select d-block my-3' required>"
                                    +"<option value=''>Quantity</option>"
                                    + purchaseQnty
                                    +"</select><div class='form-check mb-2 mr-sm-2 mb-sm-0'>"
                                    +"<button class='btn btn-primary' formaction='purchase.php' type='submit'>"+linkAction.toUpperCase()+"</button></div></form>"
                +"</td></tr> ";

//{"prodType":1,"productName":"Gardening For Wellness ","productDescription":"This is a description of test book","isActive":true,"productPrice":21.5,
//    "availableQuantity":99,"productPartner":1003,"product_id":1,"gid":null,"isbn":"78-3-16-148410-1","author":"Dave Johnson","manufacturer":null,
//    "modelNumber":null,"carrier":null,"twoInOne":false,"link":[{"action":"buy","url":"http://localhost:8081/orderservice/orders","type":"POST"}]}
//               // tableRow = tableRow +"<tr><td> Buy </td><td><a href='' onclick='buyProduct("+jsonResponse.product_id+")' >"+linkAction.toUpperCase()+"</a></td></tr> ";  


                console.log("Product_ID= "+ jsonResponse.product_id);
                console.log("link url: "+ jsonResponse.link[0].url);    

                tableRow += ' </tbody> </table> ';
                var completeTable=productResultTable_sub1+ tableRow;
                console.log("PDetailTable: "+ completeTable);
                //$('#content2').html(completeTable);
               document.getElementById("content2").innerHTML=completeTable;
           
               
            }
         };
            var url="http://localhost:8081/productservice/products/"+productId;
            console.log("url= "+ url);
            xhttp.open("GET", url, true);
            xhttp.setRequestHeader( "Accept", "application/json");
            xhttp.send();
}

function createOrder(){

     console.log('createOrder--> jsonData: '+jsonData);
     var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {


        if (this.readyState == 4 && this.status == 200) {
            alert('Order placed!');
            redirectProductSearch();
        }
     };
        var url="http://localhost:8081/orderservice/orders";
        console.log("Create Order url= "+ url);
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader( "Content-Type", "application/json");
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.send(jsonData)
        

}

 