<?php  session_start(); ?>
<script src="js/main.js"></script>

<?php
global $json_post_data;
require_once $_SERVER["DOCUMENT_ROOT"].'/Lakeshore_Client/dal/db.inc.php';
//echo 'Inside the signupHandler.php file at location '. $_SERVER["DOCUMENT_ROOT"].'/Lakeshore_Client/<br>';

if (   isset($_POST['lName']) 
    && isset($_POST['fName']) 
    && isset($_POST['userName'])
    && isset($_POST['suserPassword'])) { 
	
    $fName=    $_POST['fName'];
    $lName=    $_POST['lName'];
    $userName= $_POST['userName'];
    $userPassword=$_POST['suserPassword'];

    $post_data= array('firstName' => $fName, 'lastName' => $lName, 'email' => $userName, 'passcode'=>$userPassword);
    $json_post_data = json_encode($post_data, JSON_FORCE_OBJECT);
    
    //Check to see if user exists already
     try{
          $sql = 'SELECT customer_id as user_id '
                 .' FROM customers'
                 .' WHERE email= :email';
          $s = $pdo->prepare($sql);
          $s->bindValue(':email', $userName);
          
          $qryResult= $s->execute();
          $results = $s->fetch(PDO::FETCH_ASSOC);

          print_r($results);

          $user_id= $results["user_id"];
          //echo '<br>'.$is_valid_user.'<br>';
 
        if($user_id>0){
           
            $_SESSION["userId"]=$user_id;
            ?><script type="text/javascript">  redirectToHome(); </script>;<?php
        }
        else{
            echo '<script>';
            echo 'var jsonData = ' . json_encode($json_post_data) . ';';
            echo '</script>';
            ?><script> createCustomer()</script><?php
        }//end else
    }
    catch (PDOException $e) {
      $error = 'Error checking for existing user: ' . $e->getMessage();
      include 'dal\error.html.php';
      exit();
    }
    
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Course Task Keeper</title>
        <link rel="stylesheet" type="text/css" href="styles/main.css">
    </head>
    <body >

        <input type="hidden" name="userId" value="<?php $new_customer_id ?>"/>

    </body>
</html>

 