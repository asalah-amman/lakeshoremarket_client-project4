

<?php 
    session_start();
    require_once $_SERVER["DOCUMENT_ROOT"].'/Lakeshore_Client/dal/db.inc.php';

    if (isset($_GET['invalid'])){
        echo 'Error logging you in';
    }

    if(isset($_GET['is_existing_user'])){
        include 'existingUser.html';
    }
    
    
?>
 
<!DOCTYPE html>
<html>
<head>
  <title>Lakeshore Market</title>
  <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
 
<body>
  <div class="form">
      
      <ul class="tab-group">
        <li class="tab"><a href="#signup">Sign Up</a></li>
        <li class="tab active"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">

         <div id="login">   
          <h1>Welcome Back!</h1>
          
          <form action="loginHandler.php" method="post" >
                <div class="field-wrap">
                  <label> Email Address<span class="req">*</span></label>
                  <input type="email" required autocomplete="off" name="userName"/>
                </div>

                <div class="field-wrap">
                  <label> Password<span class="req">*</span> </label>
                  <input type="password" required autocomplete="off" name="userPassword"/>
                </div>

                <p class="forgot"><a href="forgot.php">Forgot Password?</a></p>

                <button class="button button-block" name="submit" />Log In</button>
          </form>

        </div>
          
        <div id="signup">   
          <h1>Sign Up for Free</h1>
          
          <form action="signupHandler.php" method="post" autocomplete="off">
          
                <div class="top-row">
                  <div class="field-wrap">
                    <label> First Name<span class="req">*</span></label>
                    <input type="text" required autocomplete="off"   name="fName"/>
                  </div>

                  <div class="field-wrap">
                    <label>Last Name<span class="req">*</span></label>
                    <input type="text"required autocomplete="off"   name="lName" />
                  </div>
                </div>

                <div class="field-wrap">
                  <label>Email Address<span class="req">*</span></label>
                  <input type="email"required autocomplete="off"   name="userName" />
                </div>

                <div class="field-wrap">
                  <label>Password<span class="req">*</span></label>
                  <input type="password"required autocomplete="off" name="suserPassword"/>
                </div>

                <button type="submit" class="button button-block" name="submit" />Register</button>
          </form>
        </div>  
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="js/index.js"></script>

</body>
</html>

<script src="js/main.js"></script>
