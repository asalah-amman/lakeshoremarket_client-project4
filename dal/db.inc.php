<?php
global $pdo;
try
{
  //echo 'Inside the db.inc.php file at location '. $_SERVER["DOCUMENT_ROOT"].'/Lakeshore_Client/db.inc.php<br>';
  $pdo = new PDO('mysql:host=localhost;dbname=lakeshore', 'root', '');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e)
{
  $error = 'Unable to connect to the database server.';
  include 'error.html.php';
  exit();
}
