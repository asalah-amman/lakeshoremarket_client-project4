<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="js/main.js"></script>

<?php 
   session_start();

   if(isset($_POST["itemSearch"])){
       $searchItem= $_POST["itemSearch"];

      ?><script> searchProducts("<?php echo $searchItem ?>");</script><?php
   } 
   
   if(isset($_POST["link"])){
       $productId= $_POST["link"];
        ?><script> getProductDetail("<?php echo $productId ?>");</script><?php
   }
?>
  <script>var customerId= <?php echo $_SESSION['userId'] ?></script>    
<html>
    <head>
        <title>Lakeshore Shopping ALLA</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles/main.css" >
        <link rel="stylesheet" href="styles/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    </head>
    <body>
        <h1>Lakeshore Market </h1>
        <div id="orderInfo">
            
        </div>
        <div id="search">
         <br>

            <div class="container">
                <form method="post" action="">
                    <input type='hidden' name='customerId' value='<?php echo $_SESSION['userId']; ?>'/>
                      
                  <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Item Search</label><br>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEmail3" name="itemSearch" placeholder="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                  </div>
                </form>
            </div>
        </div>
        
        <!--DIV FOR PRODUCT DISPLAY OF PRODUCT SEARCH RESULT-->
        <div id="content">
            
        </div>
        <br><br>
        
        <!--DIV FOR PRODUCT DISPLAY OF PRODUCT DETAIL-->
         <div id="content2">
            
        </div>
    
        <!--QUANTITY ORDERED-->
        <div id="number">
        </div>
    </body>
</html>



