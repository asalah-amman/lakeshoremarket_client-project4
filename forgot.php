<?php 
    if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) 
    {  

    }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Reset Your Password</title>
  <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>

<body>
    
  <div class="form">

        <h1>Reset Your Password</h1>

        <form action="forgot.php" method="post">
           <div class="field-wrap">
             <label>Email Address<span class="req">*</span></label>
             <input type="email"required autocomplete="off" name="email"/>
             <br><br><a href="index.php">Back to Home</a></p>

           </div>
           <button class="button button-block"/>Reset</button>
        </form>
  </div>
          
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="js/index.js"></script>
</body>

</html>
